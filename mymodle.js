exports.add = (x,y) => {
	return x + y;
}

// 3 funcitons sub, mul, div, sqr

exports.sub = (x,y) => {
	return x - y;
}

exports.mul = (x,y) => {
	return x * y;
}

exports.div = (x,y) => {
	return x / y;
}

exports.sqr = (a) => {
	return a * a;
}

 