var mongoose = require("mongoose");

conn_str = "mongodb://127.0.0.1:27017/tcet";
//connection to mongodb
mongoose.connect(conn_str, {useNewUrlParser:true, useUnifinedTopology:true})

const studentSchema = new mongoose.Schema({
	"name" : String,
	 "age" : Number,
	 "city" : String
});

const StudentModel = new mongoose.model("students", studentSchema);

exports.Student = StudentModel;