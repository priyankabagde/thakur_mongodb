var express = require("express");
var app = express();

const port = 8088;

var student_module = require("./student_module");
const student = student_module.Student;

app.use(express.json());

app.route("/students")

.get(async (req, res) => {
	let data = await student.find();
	console.log(data);
	res.send(data);
})
.post(async (req, res) =>{
	//console.log(req.body);
	let s = new student(req.body);
	let result = await s.save(); 
	res.send(result);
})
.put(async (req, res) =>{
	//console.log(req.body);
	console.log(req.body._id)
	console.log(req.body.name)
	//console.log(req.body.age)
	//console.log(req.body.city)
	
	let s_data = await student.updateOne({"_id": req.body._id},{
		"$set" : {
			"name" : req.body.name
			//"age" : req.body.age,
			//"city" : req.body.city
		}
	});
	res.send(s_data);
})

.delete(async (req, res) =>{
	console.log(req.query._id)
	let d_data = await student.deleteOne({_id:req.query._id})
	res.send(d_data);
})


app.listen(port, () => console.log(`Lisenting on port ${port}`));