var express = require("express");
var app = express();

const port = 8088;

app.use(express.json()); //middleware to read json data

app.get("/", (req, res) => {
	console.log("say hello");
	res.send("get method response");
})

app.post("/", (req, res) => {
	console.log(req.body);
	x = req.body.num1;
	y = req.body.num2;
	z = x * y;
	res.send({"result" : z});
})

app.listen(port, () => console.log(`Lisenting on port ${port}`));